<?php

/* @var $this yii\web\View */

use yii\grid\GridView;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Users!</h1>
    </div>

    <div class="body-content">
        <?php
        /** @var \yii\data\ActiveDataProvider $dataProvider */
        echo GridView::widget([
            'dataProvider' => $dataProvider,
        ]);
        ?>
    </div>
</div>
