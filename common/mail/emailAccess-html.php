<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/** @var string $password */
?>
<div class="verify-email">
    <p>Hello <?= Html::encode($user->first_name) ?> <?= Html::encode($user->second_name) ?>,</p>

    <p>Login: <?= Html::encode($user->username) ?></p>
    <p>Password: <?= Html::encode($password) ?></p>

</div>
